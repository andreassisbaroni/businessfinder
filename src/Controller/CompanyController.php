<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\CompanyRepository;
use App\Repository\CategoryRepository;
use App\Entity\Company;
use App\Entity\Category;

class CompanyController extends Controller
{
    /**
     * @Route("/search", methods={"POST"}, name="searchCompanies")
     */
    public function searchCompanies(Request $request, CompanyRepository $repository)
    {
    	$textSearch = $request->get('text');
    	$companies = $repository->findBySearchScreen($textSearch);

        return $this->render('companies/searchCompanies.html.twig', [
            'companies' => $companies,
            'textSearch' => $textSearch,
        ]);
    }

    /**
     * @Route("/search/{companyId}", methods={"get"}, name="aboutCompany")
     */
    public function aboutCompany(Request $request, CompanyRepository $repository, $companyId)
    {
        $company = $repository->find($companyId, null, null);

        return $this->render('companies/aboutCompany.html.twig', [
            'company' => $company,
        ]);
    }    

    /**
	 * @Route("/admin/companies/form", methods={"GET"}, name="formCreateCompanies")
     */
    public function getFormCreateCompany(Request $request, CategoryRepository $categoryRepository) {
    	$categories = $categoryRepository->findAll();

    	return $this->render('admin/companies/formCreate.html.twig', [
    		'categories' => $categories,
    	]);
    }

    /**
     * @Route("/admin/companies", methods={"GET"}, name="listAllCompanies")
     */
    public function listCompanies(Request $request, CompanyRepository $repository)
    {
        $companies = $repository->listAllCompanies();

        return $this->render('admin/companies/listCompanies.html.twig', [
            'companies' => $companies,
        ]);
    }

    /**
	 * @Route("/admin/companies", methods={"POST"}, name="createCompanies")
     */
    public function createCompany(Request $request, CategoryRepository $categoryRepository) {
    	$company = new Company();

    	$company->setTitle($request->get('title'));
    	$company->setPhone($request->get('phone'));
    	$company->setAddress($request->get('address'));
    	$company->setPostalCode($request->get('postalCode'));
    	$company->setCity($request->get('city'));
    	$company->setState($request->get('state'));
    	$company->setDescription($request->get('description'));

    	$categories = $request->get('categories');
    	foreach ($categories as $categoryId) {
    		$category = $categoryRepository->find($categoryId, null, null);
    		$company->addCategory($category);
    	}

    	$entityManager = $this->getDoctrine()->getManager();
    	$entityManager->persist($company);
    	$entityManager->flush();

    	return $this->redirectToRoute('listAllCompanies');
    }
}
