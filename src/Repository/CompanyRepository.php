<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Company::class);
    }

    public function listAllCompanies() {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.category', 'ca')
            ->addSelect('ca')
            ->getQuery()
            ->getResult();
    }

    public function findBySearchScreen($textSearch) {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.category', 'ca')
            ->addSelect('ca')
            ->orWhere('LOWER(c.title) like :text')
            ->orWhere('LOWER(c.address) like :text')
            ->orWhere('LOWER(c.postalCode) like :text')
            ->orWhere('LOWER(c.city) like :text')
            ->orWhere('LOWER(ca.description) like :text')
            ->setParameter('text', strtolower('%'.$textSearch.'%'))
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Company[] Returns an array of Company objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Company
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
