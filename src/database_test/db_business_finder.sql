-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: db_business_finder
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Auto'),(2,'Beauty and Fitness'),(3,'Entertainment'),(4,'Food and Dining'),(5,'Health'),(6,'Sports'),(7,'Travel');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Pizzaria Cuore','14 3641 0615','R. Otero, 44 - Centro','17340-000','Barra Bonita','SP','Oferece inúmeras variedades de pizza, além de terem um sabor inigualável, no momento apenas trabalha com entrega de pizza, por conta do local estar passando por reformas para melhor atender seus clientes.'),(2,'Q-Tall Pizzaria','14 3641 4404','R. Luiz Reginato, 655 - Vila Narcisa','17340-000','Barra Bonita','SP','Considerado por muitos, como a melhor pizzaria da cidade, oferece muitas opções de pizzas para seus clientes, tem um grande espaço físico para atender a grandes quantidades de clientes numa noite.'),(3,'Pizza & Tal','14 3641 4696','R. Antonio Torelli, 100 - Vila Nova','17340-000','Barra Bonita','AP','Além de ser um ótimo estabelecimento, está localizada em um dos pontos principais da cidade.'),(4,'Samacahê Viagens e Turismo','14 3641 9395','R. Prudente de Moraes, 1195 - Centro','17340-000','Barra Bonita','SP','Ideal para fazer viagens de ferias para qualquer destino nacional ou internacional.'),(5,'CVC Shopping Barra Bonita','14 3642 2002','Av. Pedro Ometto, 425 - Loja 25 / Box B 19','17340-000','Barra Bonita','SP','Sendo uma das agencias mais conhecidas da cidade, é possível encontrar ótimos preços em viagens para qualquer destino desejado.'),(6,'Academia Speed Form','14 3641 5552','R. Geraldo Fazzio, 1060 - Distrito Industrial 1','17340-000','Barra Bonita','SP','É uma das academias mais conhecidas e frequentadas da cidade, tendo também estabelecimentos em outras cidades, além de também vender suplementos alimentares'),(7,'Academia 1 Movimento','14 3641 0038','R. Teresa Gandini Bola, 263-305 - Nucleo Hab.','17340-000','Barra Bonita','SP','É a academia com mais espaço para seus clientes, sendo possível montar treinamentos de acordo com seu biotipo, e seu treinamento também será monitorado por um profissional!'),(8,'Stok Motopeças','14 3644 4817','R. Joaquim Cardia, 260 - Centro','17350-000','Igaraçu do Tietê','SP','É a melhor auto mecanica de motos da cidade, onde também é possível comprar diversos equipamentos tanto para sua moto, quanto para sua segurança.'),(9,'Cantu Auto Peças','14 3644 1566','R. Sebastiana de Barros, 145-159','17350-000','Igaraçu do Tietê','SP','Venda de peças de carros com ótimo custo beneficio.'),(10,'Biazotto Som','14 3644 1799','R. Nossa Sra. de Fátima','17350-000','Igaraçu do Tietê','SP','Ótimo lugar para instalar som em seu veículo, tudo é feito da melhor forma possível para que não aconteça nenhum problema eletrico em seu carro.');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_category`
--

DROP TABLE IF EXISTS `company_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_category` (
  `company_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`company_id`,`category_id`),
  KEY `IDX_1EDB0CAC979B1AD6` (`company_id`),
  KEY `IDX_1EDB0CAC12469DE2` (`category_id`),
  CONSTRAINT `FK_1EDB0CAC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1EDB0CAC979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_category`
--

LOCK TABLES `company_category` WRITE;
/*!40000 ALTER TABLE `company_category` DISABLE KEYS */;
INSERT INTO `company_category` VALUES (1,4),(2,4),(3,4),(4,3),(4,7),(5,3),(5,7),(6,2),(6,5),(7,2),(7,5),(7,6),(8,1),(9,1),(10,1);
/*!40000 ALTER TABLE `company_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20180513021123');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-13  0:02:18
